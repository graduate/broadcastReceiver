package com.gaurabdahal.broadcasttest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class MainReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("com.examples.broadcasttest.MSG1"))
            Toast.makeText(context, "Broadcast MSG1 Detected", Toast.LENGTH_LONG).show();
        else if (intent.getAction().equals("com.examples.broadcasttest.MSG2"))
            Toast.makeText(context, "Broadcast MSG2 Detected", Toast.LENGTH_LONG).show();
        // Phone state changed?
        if (intent.getAction().equals(TelephonyManager.ACTION_PHONE_STATE_CHANGED)) {
            TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            switch (telephony.getCallState()) {
                // Call started?
                case TelephonyManager.CALL_STATE_RINGING:
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    // Take whatever action needed - mute audio
                    break;
                // Call ended?
                case TelephonyManager.CALL_STATE_IDLE:
                    // Take whatever action needed - start playing audio again
                    break;
            }
        }
    }
}
